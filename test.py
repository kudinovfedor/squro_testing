import os
import time
import unittest
from unittest import TestCase, skip
from parameterized import parameterized

from gui_tests import *


class TestLogin(TestCase):

    driver_type = "chrome"

    def setUp(self):
        self.user = create_session(driver_type=self.driver_type)

    def tearDown(self):
        self.user.close()

    def test_successful_login(self):
        LoginPage(self.user).open_page()

        f = LoginForm(self.user)
        f.fill_email(EMAIL)
        f.fill_password(PASSWORD)
        f.submit_form()

        # todo cabinet and login form are clientarea.php
        wait_title(self.user, ClientAreaPage.title)

    def test_login_failure(self):
        p = LoginPage(self.user).open_page()

        f = LoginForm(self.user)
        f.fill_email("some"+EMAIL)
        f.fill_password(PASSWORD)
        f.submit_form()

        self.assertEqual(
            p.get_error().text,
            "Login Details Incorrect. Please try again."
        )


class TestPasswordRecovery(TestCase):

    driver_type = "chrome"

    def setUp(self):
        self.user = create_session(driver_type=self.driver_type)

    def tearDown(self):
        self.user.close()

    @skip("email sent every time")
    def test_pass_recovery_success(self):
        p = ForgotPassPage(self.user).open_page()
        f = ForgotPassForm(self.user)
        f.fill_email(EMAIL)
        f.submit_form()
        self.assertEqual(
            p.get_message().text,
            "Validation Email Sent"
        )

    def test_pass_recovery_error(self):
        p = ForgotPassPage(self.user).open_page()
        f = ForgotPassForm(self.user)
        f.fill_email("some_"+EMAIL)
        f.submit_form()
        self.assertEqual(
            p.get_error().text,
            "No client account was found with the email address you entered"
        )


class TestRegistration(TestCase):

    driver_type = "chrome"

    def setUp(self):
        self.user = create_session(driver_type=self.driver_type)

    def tearDown(self):
        self.user.close()

    @parameterized.expand([
        ["100pc", "ABCDEFGhijk123!@", "100%"],
        ["90pc", "ABCDEFGhijk123", "90%"],
        ["60pc", "ABCDEFGhijk", "60%"],
        ["30pc", "abcdefghijk", "30%"]
    ])
    def test_password_strength(self, name, password, strength_ratio):
        """
        ddt of pass strength
        :param name:
        :param password:
        :param strength_ratio:
        :return:
        """
        RegisterPage(self.user).open_page()
        f = RegisterForm(self.user)
        data = {
                "password": password,
                "confirm_password": password
        }
        f.fill_account_security(data)
        self.assertIn(strength_ratio, f.get_pass_strength().text)

    @parameterized.expand([
        ["almost_success", "", "", "", 1],
        ["passwords_not_equal", "account_security", "confirm_password", "ABCDEFGhijk123", 2],
        ["not_unique_email", "personal_information", "email_address", EMAIL, 2],
        ["empty_additional_phone", "additional_required_information", "mobile_number", "", 2]
    ])
    def test_fill_registration(self, name, block, key, value, errors_count):
        """
        ddt of reg form
        :param block: form block
        :param key: field from block
        :param value: modify selected key to set value
        :param errors_count: how many errors should be present after submit
        :return:
        """
        p = RegisterPage(self.user).open_page()
        f = RegisterForm(self.user)
        data = {
            "personal_information": {
                "first_name": "Fedor",
                "last_name": "Kudinov",
                "email_address": "some"+EMAIL,
                "phone_code": "380",
                "phone_number": "637057268"
            },
            "billing_address": {
                "company_name": "Foo",
                "street_address": "Buzz",
                "street_address_2": "Bar",
                "city": "Kiev",
                "state": "",
                "postcode": "01001",
                "country_code": "UA"
            },
            "additional_required_information": {
                "how_did_you_find_us": "Google",
                "mobile_number": "+380637057268"

            },
            "account_security": {
                "password": "ABCDEFGhijk123!@",
                "confirm_password": "ABCDEFGhijk123!@"
            },
            "click_join_our_mailing_list": True
        }
        if block and key:
            data[block][key] = value

        f.fill_form(data)
        f.submit_form()

        errors = p.get_errors()
        self.assertEqual(len(errors), errors_count)

    def test_empty_registration(self):
        p = RegisterPage(self.user).open_page()
        f = RegisterForm(self.user)
        data = {
            "personal_information": {
                "first_name": "Fedor",
                "last_name": "Kudinov"
            },
            "billing_address": {
                "street_address": "Buzz",
                "city": "Kiev"
            }
        }
        f.fill_form(data)
        f.submit_form()
        errors = p.get_errors()
        self.assertEqual(len(errors), 6)


if __name__ == "__main__":

    arg = os.environ.get("driver_type", "chrome")
    if arg in ["chrome", "firefox"]:
        TestCase.driver_type = arg
    print(TestCase.driver_type, " will be used as WebDriver")

    unittest.main()
