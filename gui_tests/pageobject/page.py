from gui_tests.settings import *


class Page:

    url = "/"

    def __init__(self, web_driver):
        self.element = web_driver

    def open_page(self):
        self.element.get(URL + self.url)
        return self


class LoginPage(Page):

    url = "/clientarea.php"
    title = "Client Area - PHPTRAVELS"

    def get_error(self):
        return self.element.s("div.alert-danger")


class RegisterPage(Page):

    url = "/register.php"
    title = "Register - PHPTRAVELS"

    def get_errors(self):
        return self.element.ss("div.alert-danger li")


class ForgotPassPage(Page):

    url = "/pwreset.php"
    title = "Lost Password Reset - PHPTRAVELS"

    def get_message(self):
        return self.element.s("div.alert-success")

    def get_error(self):
        return self.element.s("div.alert-danger")


class ClientAreaPage(Page):

    url = "/clientarea.php"
    title = "Client Area - PHPTRAVELS"


class LoginForm:

    def __init__(self, web_driver):
        self.element = web_driver.s("form.login-form")

    def fill_email(self, value):
        self.element.s("#inputEmail").send_keys(value)

    def fill_password(self, value):
        self.element.s("#inputPassword").send_keys(value)

    def submit_form(self):
        self.element.s("#login").click()


class ForgotPassForm:

    def __init__(self, web_driver):
        self.element = web_driver.s("form[action='https://phptravels.org/pwreset.php']")

    def fill_email(self, value):
        self.element.s("#inputEmail").send_keys(value)

    def submit_form(self):
        self.element.s("button[type='submit']").click()


class RegisterForm:
    def __init__(self, web_driver):
        self.element = web_driver.s("#frmCheckout")

    def fill_personal_information(self, data):
        self.element.s("#inputFirstName").send_keys(data.get("first_name", ""))
        self.element.s("#inputLastName").send_keys(data.get("last_name", ""))
        self.element.s("#inputEmail").send_keys(data.get("email_address", ""))

        # select country by phonecode
        if data.get("phone_code", ""):
            self.element.s(".flag-container").click()
            self.element.s(".country-list li[data-dial-code='{}']".format(data["phone_code"])).click()

        self.element.s("#inputPhone").send_keys(data.get("phone_number", ""))

    def fill_billing_address(self, data):

        self.element.s("#inputCompanyName").send_keys(data.get("company_name", ""))
        self.element.s("#inputAddress1").send_keys(data.get("street_address", ""))
        self.element.s("#inputAddress2").send_keys(data.get("street_address_2", ""))
        self.element.s("#inputCity").send_keys(data.get("city", ""))
        self.element.s("#stateinput").send_keys(data.get("state", ""))
        self.element.s("#inputPostcode").send_keys(data.get("postcode", ""))

        # select country by code
        if data.get("country_code"):
            self.element.s("#inputCountry option[value='{}']".format(data["country_code"])).click()

    def fill_additional_required_information(self, data):
        # select how did u find us
        if data.get("how_did_you_find_us"):
            self.element.s("#customfield1 option[value='{}']".format(data["how_did_you_find_us"])).click()
        self.element.s("#customfield2").send_keys(data.get("mobile_number", ""))

    def fill_account_security(self, data):
        self.element.s("#inputNewPassword1").send_keys(data.get("password", ""))
        self.element.s("#inputNewPassword2").send_keys(data.get("confirm_password", ""))

    def click_join_mailing_list(self):
        self.element.s("div.marketing-email-optin .bootstrap-switch").click()

    def submit_form(self):
        self.element.s("input[type='submit']").click()

    def fill_form(self, data):

        if data.get("personal_information"):
            self.fill_personal_information(data["personal_information"])

        if data.get("billing_address"):
            self.fill_billing_address(data["billing_address"])

        if data.get("additional_required_information"):
            self.fill_additional_required_information(data["additional_required_information"])

        if data.get("account_security"):
            self.fill_account_security(data["account_security"])

        if data.get("click_join_our_mailing_list"):
            self.click_join_mailing_list()

    def get_pass_strength(self):
        return self.element.s("#passwordStrengthTextLabel")
