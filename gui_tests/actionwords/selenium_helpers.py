__all__ = ["create_session",
           "execute_script",
           "open_tab",
           "wait_element",
           "wait_title",
           "wait_page_loaded"]


from selenium import webdriver
from selenium.webdriver import ActionChains as AC
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import (NoSuchElementException,
                                        ElementNotSelectableException,
                                        InvalidElementStateException)
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.remote.command import Command

from gui_tests.settings import *


# monkey patch the find_by_css_selector method :
def webdriver_s(self, value):
    return self.execute(Command.FIND_ELEMENT, {
        'using': "css selector",  # By.CSS_SELECTOR,
        'value': value})['value']


def webdriver_ss(self, value):
    return self.execute(Command.FIND_ELEMENTS, {
        'using': "css selector",  # By.CSS_SELECTOR,
        'value': value})['value']


def webelement_s(self, value):
    return self._execute(Command.FIND_ELEMENT, {
        'using': "css selector",  # By.CSS_SELECTOR,
        'value': value})['value']


def webelement_ss(self, value):
    return self._execute(Command.FIND_ELEMENTS, {
        'using': "css selector",  # By.CSS_SELECTOR,
        'value': value})['value']


WebDriver.s = webdriver_s
WebDriver.ss = webdriver_ss
WebElement.s = webelement_s
WebElement.ss = webelement_ss


def create_session(driver_type="chrome", incognito=False):

    if driver_type == "chrome":
        options = webdriver.ChromeOptions()
        if incognito:
            options.add_argument("--incognito")
        options.add_experimental_option(
            "prefs", {'profile.managed_default_content_settings.media_stream': 1})
        return webdriver.Chrome(executable_path=CHROMEDRIVERPATH, options=options)

    elif driver_type == "firefox":
        return webdriver.Firefox(executable_path=GECKODRIVERPATH)


def execute_script(web_driver, script_name=None, css="", value=""):
    scripts = {
        "scroll_to_bottom": "document.body.scrollTop = document.body.scrollHeight;",

        "send_keys": "$('{css}').val('{value}')".format(
            css=css,
            value=value),

        "click": '$("{css}").click();'.format(
            css=css)
    }
    script = scripts[script_name]
    web_driver.execute_script(script)


def open_tab(web_driver, tab):
    web_driver.switch_to_window(web_driver.window_handles[tab-1])


def wait_element(web_driver, css_selector):
    WebDriverWait(web_driver, 10).until(lambda driver: driver.s(css_selector).is_displayed())
    return web_driver.s(css_selector)


def wait_title(web_driver, title):
    WebDriverWait(web_driver, 10).until(lambda driver: title in driver.title)


def page_loaded(web_driver):
    page_state = web_driver.execute_script('return document.readyState;')
    return page_state == 'complete'


def wait_page_loaded(web_driver):
    WebDriverWait(web_driver, 10).until(lambda driver: page_loaded(driver))
